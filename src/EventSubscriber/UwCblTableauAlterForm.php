<?php

namespace Drupal\uw_cbl_tableau\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Drupal\uw_cfg_common\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblTableauAlterForm.
 */
class UwCblTableauAlterForm extends UwCblBase implements EventSubscriberInterface {

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    if ($this->checkLayoutBuilder($event, 'Tableau')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for instagram.
      $form['#validate'][] = [$this, 'uw_cbl_tableau_validation'];
    }
  }

  /**
   * Form validation for tableau.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function uw_cbl_tableau_validation(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {

    // Get the settings from the form.
    $settings = $form_state->getValue('settings', NULL);

    // If there are settings, continue to process.
    if ($settings) {

      // If there is a block, continue to process.
      if ($block = $settings['block_form']) {

        // If there is no tableau server selected, set error.
        if ($block['field_uw_tbl_server'] == NULL) {

          // Set the form error for not having a tableau server selected.
          $form_state->setErrorByName('settings][block_form][field_uw_tbl_server', 'You must select a Tableau server.');
        }
        // If there is no site name entered, set error.
        else if ($block['field_uw_tbl_site_name'][0]['value'] == '') {

          // Set the form error for not having a site name entered.
          $form_state->setErrorByName('settings][block_form][field_uw_tbl_site_name', 'You must enter a Tableau site name.');
        }
        // If there is no tableau name entered, set error.
        else if ($block['field_uw_tbl_tableau_name'][0]['value'] == '') {

          // Set the form error for not having a site name entered.
          $form_state->setErrorByName('settings][block_form][field_uw_tbl_tableau_name', 'You must enter a Tableau name.');
        }
        // If there is no tableau height entered, set error.
        else if ($block['field_uw_tbl_tableau_height'][0]['value'] == '') {

          // Set the form error for not having a tableau height entered.
          $form_state->setErrorByName('settings][block_form][field_uw_tbl_tableau_height', 'You must enter a Tableau height.');
        }
        // If there is no tableau display tabs selected, set error.
        else if ($block['field_uw_tbl_display_tabs'] == NULL) {

          // Set the form error for not having a tableau display tabs selected.
          $form_state->setErrorByName('settings][block_form][field_uw_tbl_display_tabs', 'You must select a Tableau display tabs.');
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HookEventDispatcherInterface::FORM_ALTER => 'alterForm',
    ];
  }
}
